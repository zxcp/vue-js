/** Created by Anton on 12.06.2018. */

const car = (name, model, owner, year, phone, image) => ({name, model, owner, year, phone, image});
const log = (text, type, date = new Date()) => ({text, type, date});

const cars = [
    car('Lamborgini', 'Aventador', 'Lucy', 2015, '217991', 'images/aventador.jpg'),
    car('Lamborgini', 'Centenario', 'Lena', 2016, '+7 950 825 55 55', 'images/Centenario.png'),
    car('Lamborgini', 'Forsennato', 'Inna', 2017, '+7 951 988 22 22', 'images/forsennato.jpeg'),
    car('Lamborgini', 'Murcielago', 'Zina', 2018, '+7 800 100 11 11', 'images/murcielago.jpg')
];

new Vue({
    el: '#app',
    data: {
        cars: cars,
        car: cars[0],
        carIndex: 0,
        phoneVisibility: false,
        search: '',
        modalVisibility: false,
        logs: []
    },
    methods: {
        selectCar: function(index) {
            this.car = cars[index];
            this.carIndex = index;
        },
        newOrder() {
            this.modalVisibility = false;
            this.logs.push(log(`Success order: ${this.car.name} - ${this.car.model}`, 'ok'))
        },
        cancelOrder() {
            this.modalVisibility = false;
            this.logs.push(log(`Canceled order: ${this.car.name} - ${this.car.model}`, 'cnl'))
        }
    },
    computed: {
        phoneBtnText() {
            return this.phoneVisibility ? 'Hide phone' : 'Show phone'
        },
        filteredCars() {
            return this.cars.filter(car => {
                return car.name.indexOf(this.search) > -1 || car.model.indexOf(this.search) > -1;
            })
        }
    },
    filters: {
        date(value) {
            return value.toLocaleString()
        }
    }
});